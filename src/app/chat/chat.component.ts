import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { Router } from '@angular/router';
import { IterableDiffers } from '@angular/core'
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  private SerivceId = "ISb1f79a79580243238c8439be2674e178";
  private cid;
  private channel;
  private channelName;
  private username;
  private searchText;
  private useradded = false;
  private status = false;
  setInt;
  public loading = false;

  // private searchChannelName = false;
  member = false;
  flag = true;
  private memberName;
  msg = false;
  channels: any = []
  private chat_message;
  private messages = [];
  dates = [];
  searchArray = [];
  joinedChannels: any = [];


  constructor(private dataservice: DataService, private router: Router, private _iterableDiffers: IterableDiffers) {
    this.username = localStorage.getItem("Name");
    this.retrieveJoinedChannels();
  }

  ngOnInit() {
    this.dataservice.createChannel(this.SerivceId, "General").subscribe()
    this.dataservice.addUser("General", this.username, this.SerivceId).subscribe();
    
  }

  //Create channel
  createchannels() {
    if (this.channel != undefined) {
      console.log(this.channel)
      this.channels.push(this.channel)
      this.dataservice.createChannel(this.SerivceId, this.channel).subscribe(response => {
        this.cid = response.sid;

      })
    }
    this.channel = '';
    this.status = false
  }

  //toggle for create channel
  showAddChannel() {
    if (this.status == false) {
      this.status = true;
    }
    else {
      this.status = false;
    }
  }

  sendMessage() {
    this.dataservice.sendMessages(this.SerivceId, document.getElementById("channelName").innerHTML, this.chat_message, this.username).subscribe()
    this.chat_message = '';
    setInterval(() => {
      this.getMessage(document.getElementById("channelName").innerHTML);
    }, 500)
  }

  //getting channel messages
  getMessage(cname) {
    this.channelName = cname;
    this.dataservice.retrieveMessages(this.SerivceId, cname).subscribe(res => {
      this.messages = res.messages
    })
  }

  //searching public channels
  searchChannel(event: any) {
    this.dataservice.retrievechannels(this.SerivceId).subscribe(res => res.channels.map(key => {
      const pattern = new RegExp("[a-zA-Z]*" + event.target.value + "[a-zA-Z]*");
      if (pattern.test(key.unique_name)) {
        this.searchArray.push(key.unique_name);
      }
    }
    ));
    this.searchArray = [];
  }

  //add member to channel
  addUserToChannel(cname) {
    this.dataservice.addUser(document.getElementById("channel_" + cname).innerHTML, this.username, this.SerivceId).subscribe(response => {
      console.log(response)
      this.joinedChannels.push(document.getElementById("channel_" + cname).innerHTML)
    }, err => { this.useradded = true; })
  }
  clear(e: any) {
    if (e === '') {
      this.useradded = false;
    }
  }

  //To retrive already joined channels on startup
  retrieveJoinedChannels() {
    this.loading = true;
    this.dataservice.retrievechannels(this.SerivceId).subscribe(res => res.channels.map(key => {
      this.channels.push(key.unique_name)
      this.dataservice.listChannel(this.SerivceId, this.username).subscribe(Response => {

        Response.channels.map(key1 => {
          if (key1.channel_sid === key.sid) {
            this.joinedChannels.push(key.unique_name)
            this.loading = false;
          }
        }
        )
      });
    }
    ));
  }

  signout() {
    localStorage.clear();
    this.router.navigate(['/']);
  }

}