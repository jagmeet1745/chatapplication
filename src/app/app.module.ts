import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {HttpClientModule} from '@angular/common/http'
import {FormsModule} from '@angular/forms'
import {AuthService} from './services/auth.service'
import { LoadingModule } from 'ngx-loading';

import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
} from "angular-6-social-login-v2";
import { RouterModule, Routes } from '@angular/router';
import { ChatComponent } from './chat/chat.component';
import { HttpModule } from '@angular/http';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { HeaderComponent } from './header/header.component';
import { FilterPipe } from './filter.pipe';
  const routes: Routes = [{
    path: '',
    component: LoginComponent
  },
  {
    path: 'chat',
    component: ChatComponent,
    canActivate : [AuthService]
  },
  {
    path: '**',
    component: PagenotfoundComponent
  }
  ]

// Configs 
export function getAuthServiceConfigs() {
let config = new AuthServiceConfig(
    [
      
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("951822458984-iminl8mk4l3egol25m91lrs008pigsk5.apps.googleusercontent.com")
      },
      
    ]
);
return config;
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ChatComponent,
    PagenotfoundComponent,
    HeaderComponent,
    FilterPipe,
    
  ],
  imports: [
    BrowserModule,
    SocialLoginModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    LoadingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [{
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
