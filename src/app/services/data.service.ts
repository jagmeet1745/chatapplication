import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs';
import { mapChildrenIntoArray } from '@angular/router/src/url_tree';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private data;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic QUM5NGVkOWI4ZGFhYzhlNzkxNTNjM2NkZjNiMWU1ZTI0Zjo5ZmU4OTc3MTE2YjA2ZWFjNjlkNDBmMmFiN2QyMzA2Mw=='
    })
  };
  constructor(private http: HttpClient) { }
  setData(userdata) {
    this.data = userdata;
  }
  getData() {
    return this.data;
  }
  createService(service) {

    return this.http.post<any>("https://chat.twilio.com/v2/Services", "FriendlyName=" + service, this.httpOptions);
  }
  createChannel(sid, channel) {
    return this.http.post<any>("https://chat.twilio.com/v2/Services/" + sid + "/Channels", "UniqueName=" + channel, this.httpOptions);

  }

  retrievechannels(sid): Observable<any> {
    return this.http.get<any>("https://chat.twilio.com/v2/Services/" + sid + "/Channels", this.httpOptions);

  }
  addUser(channelSid, idendity, serviceSid) {
    return this.http.post<any>("https://chat.twilio.com/v2/Services/" + serviceSid + "/Channels/" + channelSid + "/Members", "Identity=" + idendity, this.httpOptions);
  }
  getChannelId(sid, uniqueName) {
    return this.http.get<any>("https://chat.twilio.com/v2/Services/" + sid + "/Channels/" + uniqueName, this.httpOptions);

  }
  sendMessages(serviceSid, uniqueName, msg, username) {
    const body = new HttpParams().set('Unique Name', uniqueName).set('ServiceSid', serviceSid).set('Body', msg).set('From', username)
    return this.http.post<any>("https://chat.twilio.com/v2/Services/" + serviceSid + "/Channels/" + uniqueName + "/Messages", body.toString(), this.httpOptions);

  }
  retrieveMessages(sid, channelSid) {
    return this.http.get<any>("https://chat.twilio.com/v2/Services/" + sid + "/Channels/" + channelSid + "/Messages", this.httpOptions);
  }
  listAllMembers(sid, channelSid) {
    return this.http.get<any>("https://chat.twilio.com/v2/Services/" + sid + "/Channels/" + channelSid + "/Members", this.httpOptions);

  }
  listChannel(sid, username) {
    return this.http.get<any>("https://chat.twilio.com/v2/Services/" + sid + "/Users/" + username + "/Channels", this.httpOptions);

  }

}
