import { Component, OnInit } from '@angular/core';
import {
  AuthService,
  GoogleLoginProvider,
  
} from 'angular-6-social-login-v2';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor( private socialAuthService: AuthService,private router:Router,private dataservice:DataService ) {}
  tokenId = "ya29.Glz0BQB2yFHlAAIlX7zB4i882IydRmBaN3ARZ-atp0N9SDnIMl5p_HSYZrbrETWxQILThtSZZh3ULpMmKtt2UEWhSC0Dd58I9Ovqxv-U2CP_31on00xVVflRqfXDPg"
  public socialSignIn(socialPlatform : string,) {
    let socialPlatformProvider;
    
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(userData);
        // this.dataservice.setData(userData);
        localStorage.setItem("Name",userData.name)
        localStorage.setItem("Token",this.tokenId)
        // Now sign-in with userData
        this.router.navigate(['/chat'])
        // ...
            
      }
    );
    
  }

  
}